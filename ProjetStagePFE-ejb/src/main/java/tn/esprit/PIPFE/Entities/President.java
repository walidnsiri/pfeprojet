package tn.esprit.PIPFE.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "President")
@NamedQueries({
    @NamedQuery(name = "President.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.President (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxSoutener,t.nsoutener) FROM President t")
})
public class President extends Enseignant implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int nmaxSoutener;
	private int nsoutener;
	
	/*@ManyToMany(mappedBy="")
	private List<Soutenance> Soutenance;*/
	
	public President() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getNmaxSoutener() {
		return nmaxSoutener;
	}

	public void setNmaxSoutener(int nmaxSoutener) {
		this.nmaxSoutener = nmaxSoutener;
	}

	public int getnSoutener() {
		return nsoutener;
	}

	public void setnSoutener(int nsoutener) {
		this.nsoutener = nsoutener;
	}

	public President(int nmaxSoutener) {
		super();
		this.nmaxSoutener = nmaxSoutener;
		this.nsoutener = 0;
	}

	@Override
	public String toString() {
		String s="President [nmaxSoutener=" + nmaxSoutener + ", nSoutener=" + nsoutener + "]";
		s=s+super.toString();
		return s;
	}
	
	public President(String nom, int nmaxSoutener,int nsoutener) {
		super(nom);
		this.nmaxSoutener=nmaxSoutener;
		this.nsoutener=nsoutener;

		// TODO Auto-generated constructor stub
	}

	public President(int id, int cin, String nom, String prenom, int tel, String email, int nmaxSoutener,
			int nsoutener) {
		super(id, cin, nom, prenom, tel, email);
		this.nmaxSoutener = nmaxSoutener;
		this.nsoutener = nsoutener;
	}

	public int getNsoutener() {
		return nsoutener;
	}

	public void setNsoutener(int nsoutener) {
		this.nsoutener = nsoutener;
	}

	public void increment() {
		nsoutener++;
	}
	
	
}
