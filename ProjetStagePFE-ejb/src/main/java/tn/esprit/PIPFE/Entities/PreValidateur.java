package tn.esprit.PIPFE.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "Prevalidateur")
@NamedQueries({
    @NamedQuery(name = "PreValidateur.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.PreValidateur (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxValider,t.nvalider) FROM PreValidateur t")
})
public class PreValidateur extends Enseignant implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int nmaxValider;
	private int nvalider;
	@ManyToMany
	private List<Categorie> PrefValiderList;
   


	public int getNmaxValider() {
		return nmaxValider;
	}



	public void setNmaxValider(int nmaxValider) {
		this.nmaxValider = nmaxValider;
	}



	public int getnValider() {
		return nvalider;
	}



	public void setnValider(int nValider) {
		this.nvalider = nValider;
	}



	public List<Categorie> getPrefValiderList() {
		return PrefValiderList;
	}



	public void setPrefValiderList(List<Categorie> prefValiderList) {
		PrefValiderList = prefValiderList;
	}



	public PreValidateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PreValidateur(int nmaxValider) {
		super();
		this.nmaxValider = nmaxValider;
		this.nvalider = 0;
		
	}
	public PreValidateur(String nom, int NmaxValider,int nvalider) {
		super(nom);
		this.nmaxValider=NmaxValider;
		this.nvalider=nvalider;

		// TODO Auto-generated constructor stub
	}



	@Override
	public String toString() {
		String s="PreValidateur [nmaxValider=" + nmaxValider + ", nValider=" + nvalider + ", PrefValiderList="
				+ PrefValiderList + "]";
		s=s+super.toString();
		return s;	}
	
	public PreValidateur(int id,int cin,String nom,String prenom,int tel,String email,int nmaxvalider, int nvalider) {
		super(id,cin,nom,prenom,tel,email);
		this.nmaxValider = nmaxvalider;
		this.nvalider = nvalider;
		
	}



	public int getNvalider() {
		return nvalider;
	}



	public void setNvalider(int nvalider) {
		this.nvalider = nvalider;
	}
	
	
	
	
	


	
	
	
}
