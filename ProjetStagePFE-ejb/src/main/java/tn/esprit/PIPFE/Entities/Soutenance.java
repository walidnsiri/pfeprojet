package tn.esprit.PIPFE.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue(value = "Soutenance")
@NamedQueries({
    @NamedQuery(name = "Soutenance.findNoPres", query = "SELECT NEW tn.esprit.PIPFE.Entities.Soutenance (t.Id,t.Sale,t.Date_Debut,t.Havepresident) FROM Soutenance t WHERE t.Havepresident=false ")
})
public class Soutenance implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Soutenance(int id, String sale, Date date_Debut, boolean havepresident) {
		super();
		Id = id;
		Sale = sale;
		Date_Debut = date_Debut;
		Havepresident = havepresident;
	}


	@Id
	@GeneratedValue
	private int Id;
	private String Sale;
	@Temporal(TemporalType.DATE)
	private Date Date_Debut;
	private boolean Havepresident;
	public boolean isHavepresident() {
		return Havepresident;
	}

	public void setHavepresident(boolean havepresident) {
		Havepresident = havepresident;
	}

	public List<Enseignant> getEnseignants() {
		return Enseignants;
	}

	public void setEnseignants(List<Enseignant> enseignants) {
		Enseignants = enseignants;
	}

	public void setNotePresident(int notePresident) {
		NotePresident = notePresident;
	}


	private int NotePresident;
	
	@OneToOne
	private FichePFE FichePfe;
    
	@ManyToMany(mappedBy="soutenances")
	private List<Enseignant> Enseignants;
	
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getSale() {
		return Sale;
	}

	public void setSale(String sale) {
		Sale = sale;
	}

	public Date getDate_Debut() {
		return Date_Debut;
	}

	public void setDate_Debut(Date date_Debut) {
		Date_Debut = date_Debut;
	}

	

	public FichePFE getFichePfe() {
		return FichePfe;
	}

	public void setFichePfe(FichePFE fichePfe) {
		FichePfe = fichePfe;
	}
	
	public Soutenance (){
		 
	}
	
	public int getNotePresident() {
		return NotePresident;
	}


	public Soutenance(String sale, Date date_Debut, Date date_Fin) {
		super();
		Sale = sale;
		Date_Debut = date_Debut;
		
	}

	public Soutenance(int id, String sale, Date date_Debut, boolean havepresident, int notePresident, FichePFE fichePfe,
			List<Enseignant> enseignants) {
		super();
		Id = id;
		Sale = sale;
		Date_Debut = date_Debut;
		Havepresident = havepresident;
		NotePresident = notePresident;
		FichePfe = fichePfe;
		Enseignants = enseignants;
	}

	@Override
	public String toString() {
		return "Soutenance [Id=" + Id + ", Sale=" + Sale + ", Date_Debut=" + Date_Debut + ", Havepresident="
				+ Havepresident + ", NotePresident=" + NotePresident + ", FichePfe=" + FichePfe + ", Enseignants="
				+ Enseignants + "]";
	}

	public Soutenance(int id, String sale, Date date_Debut, boolean havepresident, FichePFE fichePfe) {
		super();
		Id = id;
		Sale = sale;
		Date_Debut = date_Debut;
		Havepresident = havepresident;
		FichePfe = fichePfe;
	}
	
	
}
