package tn.esprit.PIPFE.Entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;

import java.io.Serializable;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Enseignant")
@NamedQueries({
    @NamedQuery(name = "Enseignant.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.Enseignant (t.Nom,t.type_employe,t.Id) FROM Enseignant t")
})

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="t_Employe")
public class Enseignant implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int Id;
	private int Cin;
	private String Nom;
	private String Prenom;
	private int Tel;
	private String Address;
	private String Email;
	@Column(name="t_Employe", insertable = false, updatable = false)
	protected String type_employe;

	public String getType_employe() {
		return type_employe;
	}
	@ManyToMany
	private List<FichePFE>FichePFE;
	@ManyToMany
	private List<Soutenance> soutenances;
	
	@ManyToMany(mappedBy="Enseignants",fetch =FetchType.EAGER)
	private List<Ecole> Ecoles;
	
	@ManyToMany(mappedBy="Enseignants",fetch =FetchType.EAGER)
	private List<Site> Sites;
	
	@ManyToMany(mappedBy="Enseignants",fetch =FetchType.EAGER)
	private List<Departement> Departements;
	
	@ManyToMany(mappedBy="Enseignants",fetch =FetchType.EAGER)
	private List<Options> Options;
	
	
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getCin() {
		return Cin;
	}
	public void setCin(int cin) {
		Cin = cin;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public int getTel() {
		return Tel;
	}
	public void setTel(int tel) {
		Tel = tel;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public List<FichePFE> getFichePFE() {
		return FichePFE;
	}
	public void setFichePFE(List<FichePFE> fichePFE) {
		FichePFE = fichePFE;
	}
	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Enseignant(int cin, String nom, String prenom, int tel, String address, String email) {
		super();
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		Tel = tel;
		Address = address;
		Email = email;
	}
	
	public Enseignant(String nom, String type_employe, int id) {
		super();
		Nom = nom;
		this.type_employe = type_employe;
		this.Id=id;
	}
	@Override
	public String toString() {
		return "Enseignant [Id=" + Id + ", Cin=" + Cin + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Tel=" + Tel
				+ ", Address=" + Address + ", Email=" + Email + ", FichePFE="
				+ FichePFE + ", soutenances=" + soutenances + ", Sites=" + Sites
				+ ", Departements=" + Departements + ", Options=" + Options + "]";
	}
	public Enseignant(String nom) {
		super();
		Nom = nom;
	}
	public Enseignant(int id, int cin, String nom, String prenom, int tel, String address, String email) {
		super();
		Id = id;
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		Tel = tel;
		Address = address;
		Email = email;
	}
	public Enseignant(int id, int cin, String nom, String prenom, int tel, String email) {
		super();
		Id = id;
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		Tel = tel;
		Email = email;
	}
	
	
	
	
	
	
	

}
